# dvcs_study

There are 3 examples how to handle g4e data: 

## Running examples

There are 3 ways of data analysis are demostrated:

### 1. Pure python example

Pure python using uproot + awkwards arrays library. Sample of making histograms and even 3d plots in this environment

***Procs*** - can be run anywhere without installing ROOT
***Cons*** - A bit slower than compiled C++ (but MUCH faster than ROOT interpreter), needs some python uproot-foo knowledge

look at **pure_jupyter_analysis.ipynb** file (not fully working, while building some histograms)


### 2. Pure root example

This example uses C++ notebook (which works the same as pure root .C macro, but allows better visualization and documentation)

[pure_cpp_root_analysis.ipynb](https://gitlab.com/eic/escalate/plugins/dvcs_study/-/blob/master/notebooks/pure_cpp_root_analysis.ipynb)

***Procs*** - good for those who is familiar with root
***Cons*** - can work only with g4e root files

***To run:***

```bash
root --notebook
```

The python which root is built with should have jupyter and 'metakernel' packages installed. 


### 3. Jana plugin example

An analysis is made inside a plugin in Jana framework

***Procs*** - can be extended with the reconstruction code further, can use different input files, multithreaded
***Cons*** - needs Jana/eJana (eic-jana) to be installed

***To run:***

```
python3 ejana_analysis.py   # Should build the plugin and run it
```

## G4E Data structure and some related fields


```
EVENTS
+---------------------+
| event_id            |
| evt_true_q2         |
| evt_true_x          |
| evt_true_y          |
| evt_true_w2         |
| evt_true_nu         |
| evt_true_t_hat      |
| evt_has_dis_info    |
| evt_weight          |
+---------------------+

GENERATED VERTEX 
Copy of vertex data from generator
+---------------------+
|gen_vtx_count        | 
|gen_vtx_id           | 
|gen_vtx_part_count   | 
|gen_vtx_x            | 
|gen_vtx_y            | 
|gen_vtx_z            | 
|gen_vtx_time         | 
|gen_vtx_weight       | 
+---------------------+ 

GENERATED PARTICLES
Copy of particle data from generator
+---------------------+
|gen_prt_count        |
|gen_prt_id           |      # Unique number/id of this particle inside the event
|gen_prt_vtx_id       |      # This id corresponds to vertex id
|gen_prt_pdg          |      # Type of the particle
|gen_prt_trk_id       |--+   # Related track id 
|gen_prt_charge       |  |
|gen_prt_dir_x        |  |
|gen_prt_dir_y        |  |
|gen_prt_dir_z        |  |
|gen_prt_tot_mom      |  |   # Particle totoal momentum
|gen_prt_tot_e        |  |      
|gen_prt_time         |  |
|gen_prt_polariz_x    |  |
|gen_prt_polariz_y    |  |
|gen_prt_polariz_z    |  |
+---------------------+  |
                         |
                         |
TRACKS - Simulated particle tracks in our detector
+---------------------+  | 
|trk_count            |  |       # A number of tracks in an event
|trk_id               | -+-+    # Unique number/id of this particle inside the event
|trk_pdg              |  | |    # Type of the particle
|trk_parent_id        | -+ |    # Parent id of the particle
|trk_create_proc      |    |
|trk_level            |    |    # 0 - generated particles, 1 - daughters of gen, 2 - daughtrs. of daughtrs. etc
|trk_vtx_x            |    |    # Particle origin vertex position
|trk_vtx_y            |    |
|trk_vtx_z            |    |
|trk_vtx_dir_x        |    |    # Track direction x,y,z (unit vector)
|trk_vtx_dir_y        |    |
|trk_vtx_dir_z        |    |
|trk_mom              |    |    # Track momentum
+---------------------+    |
HITS in the detector       |
+---------------------+    |
|hit_count            |    |    # Number of hits in the event
|hit_id               |    |
|hit_trk_id           | ---+
|hit_ptr_id           |
|hit_parent_trk_id    |
|hit_vol_name         |         # Volume name where hit occured
|hit_x                |
|hit_y                |
|hit_z                |
|hit_e_loss           |
+---------------------+
```

If event hierarchy is something like this:

```
event 1
    track 1 - electron 10GeV
        hit_11
        hit_12
        hit_13
    track 2 - proton   100GeV
        hit_21
        hit_22
```

In g4e output this looks like this:

```
event_id  = 1

trk_count = 2
trk_id    = [1,  2]     
trk_pdg   = [11, 2112]
trk_mom   = [10, 100]
trk_...   = [...    ]  - all arrays will be of 2 elements

hit_count     = 5
hit_...       = [...] - all arrays are of 5 elements
hit_id        = [1,  2,   3,    4,  5]
hit_trk_id    = [1,  1,   1,    2,  2] - link to tracks
hit_trk_index = [0,  0,   0,    1,  1] - index in track array
hit_z         = [10, 100, 200, -1, -5]
```

### IDs vs Indexes

(Only works with newer G4E)

There are two different fields **hit_trk_id** and **hit_trk_index**:

- hit_trk_id - represents a track id issued by Geant4 (unique inside an event). hit_trk_id-s 
   may not correspond to the order of how they are written in trk_... arrays and not all tracks are written

- hit_trk_index - track index in trk_... array. So it is safe to use like this:    
   ```c++
   for(int i=0; i < hit_count; i++) {
      // Get parent track momentum
      double momentum = trk_mom[hit_trk_index[i]];
   }
   ```


### Dimentions

All dimentions - mm
All energies and momentums - GeV
