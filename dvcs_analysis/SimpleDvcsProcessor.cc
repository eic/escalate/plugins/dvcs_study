#include "TLorentzVector.h"

#include <fmt/core.h>

#include <JANA/JEvent.h>
#include <dis/functions.h>
#include <MinimalistModel/McFluxHit.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <MinimalistModel/McTrack.h>
#include <MinimalistModel/DisInfo.h>
#include <ejana/plugins/io/beagle_reader/BeagleEventData.h>

#include <dis/functions.h>
#include "SimpleDvcsProcessor.h"

#include <TClingRuntime.h>
#include <fmt/format.h>             // For format and print functions
#include <ejana/EServicePool.h>


// TODO repalce by some 'conventional' PDG info provider. Like TDatabasePDG
//==================================================================
Double_t MassPI = 0.139570,
        MassK = 0.493677,
        MassProton = 0.93827,
        MassNeutron = 0.939565,
        MassE = 0.00051099895,
        MassMU = 0.105658;
//==================================================================

using namespace std;
using namespace fmt;


void SimpleDvcsProcessor::Init() {
    ///  Called once at program start.
    print("GeneralEmcalAnalysis::Init()\n");

    // initialize class that holds histograms and other root objects
    // 'services' is a service locator, we ask it for TFile
    root.init(services.Get<TFile>());

    // Ask service locator for parameter manager. We want to get this plugin parameters.
    // JParameterManager allows to set parameters from command line or python with
    // -Pplugin_name:param_name=value  flags
    auto pm = services.Get<JParameterManager>();

    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    verbose = 0;
    pm->SetDefaultParameter("dvcs_analysis:verbose", verbose,
                            "Plugin output level. 0-almost nothing, 1-some, 2-everything");

    // Cut that leaves only truly recoil electrons
    only_recoil = true;
    pm->SetDefaultParameter("dvcs_analysis:only_recoil", only_recoil,
                            "Cut that leaves only truly recoil electrons");

    // electron beam energy GeV (for X,Y,Q2 calculation)
    e_beam_energy=5;
    pm->SetDefaultParameter("dvcs_analysis:e_beam_energy", e_beam_energy,
                            "Electron beam energy [GeV] (for X,Y,Q2 calculation)");

    // ion beam energy GeV (for X,Y,Q2 calculation)
    ion_beam_energy = 50;
    pm->SetDefaultParameter("dvcs_analysis:ion_beam_energy", e_beam_energy,
                            "Ion beam energy [GeV] (for X,Y,Q2 calculation)");

    // Zero total statistics
    _stat.events_count = 0;
    _stat.total_el_in_barrel = 0;
    _stat.total_el_in_elcap = 0;
    _stat.total_el_in_ioncap = 0;
    _stat.recoil_el_in_barrel = 0;
    _stat.recoil_el_in_elcap = 0;
    _stat.recoil_el_in_ioncap = 0;
    _stat.el_total_count = 0;

    // print out the parameters
    if (verbose) {
        print("Parameters:\n");
        print("  dvcs_analysis:verbose         = {0}\n", verbose);
        print("  dvcs_analysis:only_recoil     = {0}\n", only_recoil);
        print("  dvcs_analysis:e_beam_energy   = {0}\n", verbose);
        print("  dvcs_analysis:ion_beam_energy = {0}\n", verbose);
    }
}

const minimodel::McGeneratedParticle * SimpleDvcsProcessor::GetRecoilElectron(const std::shared_ptr<const JEvent> &event) {
    /// Returns

    // In beagle the first electron is alwais recoil
    auto gen_parts = event->Get<minimodel::McGeneratedParticle>();
    for(auto particle: gen_parts) {
        if(particle->is_stable && particle->pdg == 11 && particle->charge < 0) return particle;
    }

    // not found
    return nullptr;
}

void SimpleDvcsProcessor::Process(const std::shared_ptr<const JEvent> &event) {
    ///< Called every event.
    using namespace fmt;
    using namespace std;
    using namespace minimodel;

    // std::lock_guard<std::recursive_mutex> locker(root_out.lock);
    _stat.events_count++;

    // >oO debug printing
    if ( (event->GetEventNumber() % 1000 == 0 && verbose > 1) || verbose >= 3) {
        print("\n--------- EVENT {} --------- \n", event->GetEventNumber());
    }

    auto tracks = event->Get<minimodel::McTrack>();
    auto hits = event->Get<minimodel::McFluxHit>();
    std::unordered_set<uint64_t> track_ids_in_ion_emcal;
    std::unordered_set<uint64_t> track_ids_in_el_emcal;
    std::unordered_set<uint64_t> track_ids_in_barrel_emcal;

    // Do we have true DIS info ?
    double x_true  = 0;
    double y_true  = 0;
    double q2_true = 0;
    if(event->GetFactory<minimodel::DisInfo>()) {
        auto dis_info = event->GetSingle<minimodel::DisInfo>();
        x_true =  dis_info->x;
        y_true =  dis_info->y;
        q2_true = dis_info->q2;

        root.h2_xq2_true->Fill(x_true, q2_true);
        root.h2_xq2_true_log->Fill(log10(x_true), log10(q2_true));
    }

    auto gen_recoil_el = GetRecoilElectron(event);

    // Loop over hits
    for (auto hit: hits) {

        // Look at hits at hadron EMCAL
        if (ej::StartsWith(hit->vol_name, "ci_EMCAL")) {
            track_ids_in_ion_emcal.insert(hit->track_id);   // Save the track id for further track processing

            // Fill some histograms
            root.h2_xy_hits_ioncap->Fill(hit->x, hit->y);
            root.h1_z_hits_any->Fill(hit->z);
        }

        // Look at hits at electron EMCAL
        if (ej::StartsWith(hit->vol_name, "ce_EMCAL")) {
            track_ids_in_el_emcal.insert(hit->track_id);    // Save the track id for further track processing

            // Fill some histograms
            root.h2_xy_hits_elcap->Fill(hit->x, hit->y);
            root.h1_z_hits_any->Fill(hit->z);
        }

        // Look at hits at Barrel EMCAL
        if (ej::StartsWith(hit->vol_name, "cb_EMCAL")) {
            track_ids_in_barrel_emcal.insert(hit->track_id);   // Save the track id for further track processing

            // Fill some histograms
            root.h2_xy_hits_barrel->Fill(hit->x, hit->y);
            root.h1_z_hits_barrel->Fill(hit->z);
            root.h1_z_hits_any->Fill(hit->z);
        }
    }

    // Count electrons
    int el_count = 0;           // Count of electrons in event
    int el_emcal_count = 0;     // Count of electrons that got to any emcal
    int el_elcap_count = 0;     // Count of electrons in Electron Endcap EMCAL


    // LOOP OVER TRACKS
    for(auto track: tracks) {

        // CUTS:
        if (track->pdg != 11) continue;       // Take only electrons for now
        if (track->parent_id != 0) continue;  // Take only particles from a generator

        TLorentzVector lv;
        lv.SetXYZM(track->px, track->py, track->pz, MassE);

        el_count++;
        _stat.el_total_count++;

        // Fill histograms for all electrons
        root.h1_all_el_e_tot->Fill(lv.E());
        root.h1_all_el_theta->Fill(lv.Theta()*57.2958);


        // For Beagle the first stable electron is always a recoil electron
        // actually, the first stable particle is the recoil electron, so we check if that is beagle
        bool is_recoil = gen_recoil_el!= nullptr && track->id == gen_recoil_el->trk_id;

        // P.S. if it is not beagle we have to implement some search logic. We will do this later

        // So what calorimeter this track in?
        bool is_in_ion_emcal = track_ids_in_ion_emcal.count(track->id);
        bool is_in_barrel_emcal = track_ids_in_barrel_emcal.count(track->id);
        bool is_in_el_emcal = track_ids_in_el_emcal.count(track->id);

        // Update statistics per EMCAL
        if(is_in_ion_emcal) _stat.total_el_in_ioncap ++;
        if(is_in_barrel_emcal) _stat.total_el_in_barrel++;
        if(is_in_el_emcal) {
            _stat.total_el_in_elcap++;
            el_elcap_count++;
        }

        // Statistics for recoil
        if(is_recoil) {
            // Update statistics per EMCAL for recoil only
            if(is_in_ion_emcal)    _stat.recoil_el_in_ioncap ++;
            if(is_in_barrel_emcal) _stat.recoil_el_in_barrel++;
            if(is_in_el_emcal)     _stat.recoil_el_in_elcap++;
        }

        // CUTS:
        // filter only tracks that are in calorimeters...
        if(!is_in_ion_emcal && !is_in_barrel_emcal && ! is_in_el_emcal) continue;

        el_emcal_count++;

        // >oO Verbose debug printing
        if (verbose >= 3) {
            print("track in any emcal: track {:<5} px: {:<11} py: {:<11} pz: {:<11} pt: {:<11}  ptot: {:<11}\n",
            track->pdg, track->px, track->py, track->pz, lv.Pt(), lv.P());
        }

        // calculate X, Y and Q2 with electron method
        double x_em, y_em, q2_em;
        double Empz_el = lv.E() - lv.Pz();
        dis::GEN_El_XYQ2(lv.E(), lv.Theta(), e_beam_energy, ion_beam_energy, x_em, y_em, q2_em);

        // fill tree for all electrons, recoil or not and for all EMCALs
        root.el_record().event_index = event->GetEventNumber();
        root.el_record().p.SetXYZM(track->px, track->py, track->pz, MassE);
        root.el_record().is_recoil = is_recoil;
        root.el_record().is_in_ioncap = is_in_ion_emcal;
        root.el_record().is_in_elcap = is_in_el_emcal;
        root.el_record().is_in_barrel = is_in_barrel_emcal;
        root.el_record().x_true = x_true;
        root.el_record().x_em = x_em;
        root.el_record().y_true = y_true;
        root.el_record().y_em = y_em;
        root.el_record().q2_true = q2_true;
        root.el_record().q2_em = q2_em;
        root.tree_rec_e->Fill();

        // CUTS:
        // leave only truly recoil electron
        if(only_recoil && !is_recoil) continue;
        if(!is_in_el_emcal) continue;

        // fill histograms
        root.h2_xq2_em_log->Fill(log10(x_em), log10(q2_em));
        root.h2_xq2_em->Fill(x_em, q2_em);
        root.h1_x_em->Fill(x_em);
        root.h1_x_em_log->Fill(x_em);
        root.h1_y_em->Fill(y_em);
        root.h1_empz->Fill(Empz_el);
        root.h1_el_e_tot->Fill(lv.Energy());
        root.h1_el_theta->Fill(lv.Theta()*57.2958);
        root.h2_q2em_vs_true->Fill(q2_true, q2_em);
        root.h2_q2em_vs_true->Fill(log10(q2_true), log10(q2_em));

        root.h2_q2diff_vs_true->Fill(q2_true, (q2_true - q2_em)/q2_true);
        root.h2_q2diff_vs_true_log->Fill(log10(q2_true), (q2_true - q2_em)/q2_true);
        root.h2_q2em_vs_true->Fill(q2_true, q2_em);
        root.h2_q2em_vs_true_log->Fill(log10(q2_true), log10(q2_em));
    }

    // Fill counts per event histos
    root.h1_emcal_el_num->Fill(el_emcal_count);
    root.h1_all_el_num->Fill(el_count);
    root.h1_elcap_el_num->Fill(el_elcap_count);
}


void SimpleDvcsProcessor::Finish() {

    ///< Called after last event of last event source has been processed.
    print("EmcalAnalysis::Finish(). Statistics:\n");
    print("  events_count         : {} \n", _stat.events_count );
    print("  total  el in barrel  : {} \n", _stat.total_el_in_barrel );
    print("  total  el in elcap   : {} \n", _stat.total_el_in_elcap  );
    print("  total  el in ioncap  : {} \n", _stat.total_el_in_ioncap );
    print("  recoil el in barrel  : {} \n", _stat.recoil_el_in_barrel );
    print("  recoil el in elcap   : {} \n", _stat.recoil_el_in_elcap  );
    print("  recoil el in ioncap  : {} \n", _stat.recoil_el_in_ioncap );
    print("  electrons total      : {} \n", _stat.el_total_count     );

}


