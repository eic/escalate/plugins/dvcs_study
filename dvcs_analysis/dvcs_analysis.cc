#include <JANA/JFactoryGenerator.h>

#include "SimpleDvcsProcessor.h"

extern "C"
{
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->Add(new SimpleDvcsProcessor(app));
    }
}
