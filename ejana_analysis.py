# Make sure pyjano is installed in your system
# do:
#     pip install --upgrade pyjano          # for conda, venv or root install
#     pip install --user --upgrade pyjano   # for user local install
#
#
import pyjano
from pyjano.jana import Jana, PluginFromSource

print(pyjano.__file__)

mstruct_general = PluginFromSource('./dvcs_analysis')   # Name will be determined from folder name
                                                         # add name=<...> for custom name

mstruct_general.builder.config['cxx_standard'] = 17

jana = Jana(nevents=1000, output='dvcs_analysis.root')
jana.plugin('g4e_reader')\
    .plugin('dis')

# Parameters:
#     verbose      - Plugin output level. 0-almost nothing, 1-some, 2-everything
#     only_recoil  - Cut that leaves only truly recoil electrons
# Beams energies. Defaults are 10x100 GeV
#     e_beam_energy    -  Energy of colliding electron beam");
#     ion_beam_energy  -  Energy of colliding ion beam");
jana.plugin(mstruct_general, verbose=1)

jana.source('sample_data/g4e_output.root')
jana.run()
